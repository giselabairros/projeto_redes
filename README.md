### Projeto de Redes de Computadores ###

Projeto desenvolvido por Gisela Bairros e Guilherme Halal.

### Servidor FTP ###

Versão 1: Servidor FTP que atenda minimamente os comandos disparados pelo cliente padrão Linux, seguindo **RFC 959**:

* USER
* QUIT
* PORT
* PASV
* TYPE
* MODE
* STRU
* RETR
* STOR
* NOOP
* LIST

Versão 2: Implementação de threads (clientes simultâneos).

Versão 3: QoS

### Executando ###

Para compilar execute o comando: gcc main.c servidor.c -o servidor -lpthread

Logo, execute: ./servidor numero_da_porta taxa


Para conectar a partir da mesma máquina, execute: ftp localhost numero_da_porta

Para conectar a partir de outra máquina, execute: ftp IP_externo numero_da_porta

### Observações ###

“Este projeto foi desenvolvido integralmente pela equipe, sem ajuda não autorizada de alunos não membros do projeto no processo de codificação.”

Para auxiliar o desenvolvimento foram utilizados como referência:

https://gist.github.com/ozcanovunc/203a6479dd5cbc322d69
https://github.com/Siim/ftp/blob/master/server.c
https://github.com/mrleiju/FTPd/blob/master/ljftpd.c
https://gist.github.com/browny/5211329
