//Integrantes: Gisela Bairros e Guilherme Halal
//Servidor FTP

#include "servidor.h"

//Tratamento de respostas servidor (foi feito a partir um método
//encontrado na internet - https://github.com/mrleiju/FTPd/blob/master/ljftpd.c.
void mensagens_servidor(int num_msg, int conexao, char *info){
    static char mensagem[TAM_BUFFER+1] = "";

    switch(num_msg) {
        case 150:
            strncpy(mensagem, "150 Dados chegando.\r\n", TAM_BUFFER);
            break;
        case 200:
            strncpy(mensagem, "200 Sucesso ao executar o comando.\r\n", TAM_BUFFER);
            break;
        case 202:
            strncpy(mensagem, "202 Comando não foi implementado.\r\n", TAM_BUFFER);
            break;        
        case 220:
            strncpy(mensagem, "220 Servidor FTP.\r\n", TAM_BUFFER);
            break;
        case 221:
            strncpy(mensagem, "221 Adeus.\r\n", TAM_BUFFER);
            break;
        case 215:
            strncpy(mensagem, "215 UNIX Type: L8\r\n", TAM_BUFFER);
            break;
        case 226:
            strncpy(mensagem, "226 Dados enviados OK.\r\n", TAM_BUFFER);
            break;
        case 227:
            strncpy(mensagem, "227 Entrando no modo passivo (", TAM_BUFFER);
            strncat(mensagem, info, strlen(info));
            strncat(mensagem, ").\r\n", 4);
            break;
        case 230:
            strncpy(mensagem, "230 Login efetuado. USERNAME: ", TAM_BUFFER);
            strncat(mensagem, info, strlen(info));
            break;
        case 331:
            strncpy(mensagem, "331 Por favor especifique uma senha.\r\n", TAM_BUFFER);
            break;        
        case 451:
            strncpy(mensagem, "451 Ação requisitada abortada: Erro local em processamento.\r\n", TAM_BUFFER);
            break; 
        case 504:
            strncpy(mensagem, "504 Comando não implementado para o parâmetro.\r\n", TAM_BUFFER);
            break;        
        case 5301:
            strncpy(mensagem, "530 Login incorreto.\r\n", TAM_BUFFER);
            break;
        case 5302:
            strncpy(mensagem, "530 Por favor faça login com USER.\r\n", TAM_BUFFER);
            break;
        case 550:
            strncpy(mensagem, "550 Falha ao abrir ou criar o arquivo.\r\n", TAM_BUFFER);
            break;
        default:
            // Mensagem de número não suportado.
            return;
    }
    write(conexao, mensagem, strlen(mensagem));
}

//Inicia a variável sockaddr_in
//Baseou-se também em https://gist.github.com/browny/5211329
void inicializa_sockaddr_in(struct sockaddr_in *endereco_socket_conexao, char *endereco_IP, unsigned int porta){
    //Preenche com 0s
    memset(endereco_socket_conexao, 0, sizeof(struct sockaddr_in));
    endereco_socket_conexao->sin_family = AF_INET;
    endereco_socket_conexao->sin_port = htons(porta);

    if(endereco_IP == NULL){
        endereco_socket_conexao->sin_addr.s_addr = htonl(INADDR_ANY);
    } else {
        endereco_socket_conexao->sin_addr.s_addr = inet_addr(endereco_IP);
    }
}

//Cria socket no endereço de socket recebido como parâmetro
//Baseou-se também em https://gist.github.com/browny/5211329
int cria_socket(struct sockaddr_in *endereco_socket) {
    //Cria socket usando STREAM (indicado na implementação
    //mínima no RFC 959), e protocolo TCP
    int novo_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    
    //Tratamento de erros
    if(novo_socket == -1) {
        fprintf(stderr, "Erro: Socket iniciado incorretamente [1].\n");
        return -1;
    }
    
    int aux = 1;
    //Seta opções para o socket
    if(setsockopt(novo_socket, SOL_SOCKET, SO_REUSEADDR, &aux, sizeof(aux))) {
        fprintf(stderr, "Erro: Socket iniciado incorretamente [2].\n");
        return -1;
    }
    
    //Ligar novo_socket a endereco_socket usando bind(), com tratamento de erros
    if(bind(novo_socket, (struct sockaddr*)endereco_socket, sizeof(struct sockaddr_in)) == -1) {
        fprintf(stderr, "Erro: Bind falhou.\n");
        close(novo_socket);
        return -1;
    }

    return novo_socket;
}

//Autenticação - É o que faz quando recebe o comando USER, mas a
//especificação do trabalho não exige real autenticação, 
//de modo que é necessário apenas reconhecer o comando para
//que se possa usar o cliente padrão linux
int autenticacao(int conexao) {
    char buffer[TAM_BUFFER +1] = "";
    read(conexao, buffer, TAM_BUFFER);
    //5 caracteres porque comandos que levam argumentos
    //tem um espaço ao final
    if(strncmp(buffer, "USER ", 5) == 0) {
        mensagens_servidor(230, conexao, buffer +5);
        return 0;
        //Aqui que ficaria a real autenticacao com senha
        //caso fosse exigido (usando PASS)
    }
    mensagens_servidor(5302, conexao, NULL);
    return 2;
}

//Analisa e separa os parâmetros do comando PORT e extrai
//o IP e porta que o cliente está escutando
//Argumentos do comando PORT
//Recebe(192,0,0,1,42,24) <- IP do cliente + porta 
//Dois últimos valores -> converte para hexadecimal e concatena, logo converte para decimal de volta
void port(char *buffer, struct sockaddr_in *endereco_cliente, int conexao) {
    unsigned int i = 0;
    unsigned int port1, port2;
    char hex1[10], hex2[10];
    int porta;
    int length = strlen(buffer);
    if (buffer){
        for(int c = 0; i < length; i++){
            if (buffer[i] == ','){
                c++;
                buffer[i] = '.';
                if (c == 4) {
                    //Final do IP
                    buffer[i] = '\0';
                    //Início da porta
                    i++;
                    break;
                }
            }
        }
        //Guarda o IP do cliente atual para teste de alocação de taxa
        strncpy (argsglobal->IPcliente, buffer+5, 16);

        //consulta lista de ips 
        //estava funcionando tem que ver com calma esse loop porque parece que entre num loop ifinito

        bool achei = false;
        if(!argsglobal->alocado){
            for (int t = ips-1; t>=0; t--){
                if (strcmp(argsglobal->IPcliente, taxIP[t].ip)==0){
                    if (atualizaTaxa(1, taxIP[t].taxa)){
                    argsglobal->taxaCliente = taxIP[t].taxa;
                    printf("Cliente IP %s definido para taxa %1.f kbps.\n", argsglobal->IPcliente, argsglobal->taxaCliente);
                    achei = true;
                    argsglobal->alocado=true;
                    break;
                    }
                else{
                    printf("Taxa máxima do servidor excedida.\nTente novamenteo mais tarde\n");
                    quit(conexao);
                    return;
                }
                }
            }
            if(!achei){
                if (atualizaTaxa(1, taxaPadraoCliente)){
                    printf("IP cliente %s não encontrado. Atribuida taxa padrão %1.f kbps.\n", argsglobal->IPcliente, taxaPadraoCliente);
                    argsglobal->taxaCliente = taxaPadraoCliente;
                    argsglobal->alocado=true;

                }
                else{
                    printf("Taxa máxima do servidor excedida.\nTente novamenteo mais tarde\n");
                    quit(conexao);
                    return;
                }
            }
        }
        

        //Recebe o primeiro valor de porta em port1 e o segundo em port2
        sscanf(buffer + i, "%d,%d", &port1, &port2);

        sprintf(hex1,"%x", port1);
        sprintf(hex2,"%x", port2);

        strcat(hex1, hex2);
        porta = (int)strtol(hex1, 0, 16);
        //printf("TESTE:\n--%d\n", porta);
        //Inicializa o socket para conexão de dados enviando como porta o decimal
        //dos dois hexadecimais concatenados.
        inicializa_sockaddr_in(endereco_cliente, buffer + 5, porta);
    }
     argsglobal->pass = false;
}

//PASV é o contrário
//Eu escolho as portas -> Envia(192,0,0,1,42,24), com portas escolhidas
//Listen na porta escolhida
//Ativa modo passivo*/

char *pasv(struct arg1 *endereco_cliente) {

    //1º pegar IP do servidor

    struct ifreq referenciaIP; //usado para receber o IP da subrede do SO
    int fdescr;
    char * IPServ = (char*) malloc(20*sizeof(char*));
    fdescr = socket(AF_INET,SOCK_DGRAM,0);
    
    // Determina o IP da subrede  como interface para servidor passivo:

    //recupera enderteço IP do hardware onde roda o serv.
    // fonte http://www.microhowto.info/howto/get_the_ip_address_of_a_network_interface_in_c_using_siocgifaddr.html
    strncpy(referenciaIP.ifr_name, "wlp1s0", IFNAMSIZ-1);

    if (ioctl(fdescr,SIOCGIFADDR,&referenciaIP)==-1) {
        close(fdescr);
        fprintf(stderr, "Erro: na obtenção do IP do servidor.\n");
        exit(-1);
    }

    close(fdescr);
    strcpy(IPServ, inet_ntoa(((struct sockaddr_in *)&referenciaIP.ifr_addr)->sin_addr));
    
    //2º criar uma porta
    int pasvport = (rand() % 30000) + 30000;

    //3º criar a conexão com essa porta
    struct sockaddr_in socketPASV;
    bzero(&socketPASV, sizeof(socketPASV));
    socketPASV.sin_family = AF_INET;
    socketPASV.sin_port = htons(pasvport);
    socketPASV.sin_addr.s_addr = inet_addr(IPServ);

    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock<0){
        fprintf(stderr, "Erro: criação do socket falhou.\n");
        exit(-1);
    }

    int b = bind(sock, (struct sockaddr*)&socketPASV, sizeof(socketPASV));
    if (b == -1){
        fprintf(stderr, "Erro: Bind falhou.\n");
        close(sock);
        exit(-1);
    }

    int l = listen(sock, 4);
    if (l<0){
        fprintf(stderr, "Erro: Escuta falhou.\n");
        close(sock);
        exit(-1);
    }


    //4ºtraduzir o endereço e a porta para informar ao cliente
        int length = strlen(IPServ);
        int i = 0;
        char hex1[10], hex2[10];
        int porta;
        for(int c = 0; i < length; i++){
            if (IPServ[i] == '.'){
                c++;
                IPServ[i] = ',';
                if (c == 4){          
                    break;
                }
            }
        }
        //
        char hex[4];
        sprintf(hex, "%X", pasvport);
        char ini[3];
        char fim[3];
        char port[8];
        sprintf(ini, "%c%c",hex[0],hex[1]);
        sprintf(fim, "%c%c",hex[2],hex[3]);
        int x = (int)strtol(ini, NULL, 16);
        int y = (int)strtol(fim, NULL, 16);
        sprintf(port, ",%d,%d",x ,y);
        strcat(IPServ, port);

    //5º atualiza dos dados da conexão
        argsglobal->socket_dados = sock;
        argsglobal->pass = true;

    //6º retorna ip do servidor e a porta no formato (h1,h2,h3,h4,p1,p2).
return IPServ;
}


//Cria um novo socket ligado a porta e IP local e conecta ao IP e porta
//do socket recebido como parâmetro
int cria_conexao_dados (const struct sockaddr_in *endereco_cliente) {
    struct sockaddr_in endereco_socket;
    inicializa_sockaddr_in(&endereco_socket, NULL, PORTA_DADOS);
    int conexao = cria_socket(&endereco_socket);
    int endereco_socket_len = sizeof(endereco_socket);

    //Tratamento de erros
    if(conexao < 0) {
        fprintf(stderr, "Erro: Não foi possível criar socket.\n");
        exit(1);
    }
    if (argsglobal->pass == false){
        if(connect(conexao, (struct sockaddr *)endereco_cliente, sizeof(struct sockaddr_in)) == -1) {
            fprintf(stderr, "Erro: Conexão de dados falhou.\n");
            close(conexao);
            exit(1);
        }
    }
    else{
        conexao = accept(argsglobal->socket_dados, (struct sockaddr*)&endereco_socket, &endereco_socket_len);
    }
    return conexao;
}


//Comando ls - Lista os arquivos contidos no diretório
//e suas informações
void list(const struct sockaddr_in *endereco_cliente, char *diretorio){
    
    char dados[TAM_BUFFER] = "";
    //Comando que lista com detalhes
    char comando[TAM_BUFFER] = "ls -l";
    int conexao = cria_conexao_dados(endereco_cliente);

    if(diretorio) {
        strncat(comando, diretorio, TAM_BUFFER - strlen(comando) -1);
    }
    //Abre arquivo com o retorno do comando ls -l no diretorio
    FILE *f = popen(comando, "r");
    int tamanho = 0;
    if(f == NULL) {
        fprintf(stderr, "Erro: Falha ao executar comando.\n");
        exit(1);
    }
    //Copia e retorna os dados do comando ls -l
    while(fgets(dados, sizeof(dados)-1, f) != NULL) {
        tamanho = strlen(dados);
        strncpy(dados + tamanho -1, "\r\n", 3);
        write(conexao, dados, strlen(dados));
    }
    //Fecha o ponteiro para o arquivo e o socket
    pclose(f);
    close(conexao);
}

//Comando get - RETR
int get(int conexao, const struct sockaddr_in *endereco_cliente, char *nome_arquivo) {
    //Tratamento de erros
    if(nome_arquivo == NULL) {
        fprintf(stderr, "Erro: Nome do arquivo é nulo [NULL].\n");
        return 1;
    }
    FILE *arquivo = fopen(nome_arquivo, "rb");
    if(arquivo == NULL) {
        fprintf(stderr, "Erro: Não foi possível encontrar o arquivo %s.\n", nome_arquivo);
        return 2;
    }
    mensagens_servidor(150, conexao, NULL);

    char dados[TAM_BUFFER] = "";
    int conexao_dados = cria_conexao_dados(endereco_cliente);

    //tamanho é a quantidade de bytes lidos do arquivo.
    int tamanho = fread(dados, 1, TAM_BUFFER, arquivo);

    //Escreve os dados
    while(tamanho > 0) {
        write(conexao_dados, dados, tamanho);
        tamanho = fread(dados, 1, TAM_BUFFER, arquivo);
    }
    fclose(arquivo);
    close(conexao_dados);
    return 0;
}

//Comando put - STOR
//Tem que fazer o oposto do que fiz acima
int put(int conexao, const struct sockaddr_in *endereco_cliente, char *nome_arquivo) {
    if(nome_arquivo == NULL) {
        fprintf(stderr, "Erro: Nome do arquivo é nulo [NULL].\n");
        return 1;
    }
    FILE *arquivo = fopen(nome_arquivo, "wb");
    if(arquivo == NULL) {
        fprintf(stderr, "Erro: Não foi possível criar o arquivo %s.\n", nome_arquivo);
        return 2;
    }
    mensagens_servidor(150, conexao, NULL);

    char dados[TAM_BUFFER] = "";
    int conexao_dados = cria_conexao_dados(endereco_cliente);

    //Tamanho é a quantidade de bytes lidos da conexão de dados (buffer)
    int tamanho = read(conexao_dados, dados, TAM_BUFFER);
    while(tamanho > 0) {
        fwrite(dados, 1, tamanho, arquivo);
        tamanho = read(conexao_dados, dados, TAM_BUFFER);
    }
    fclose(arquivo);
    close(conexao_dados);

    return 0;
}

//Separado do método principal para receber mais de uma conexão a partir
//de threads (chamando esse método)
void *aceitando_conexoes (void *arg) {
    //Variaveis auxiliares
    int tamanho_comando = 0;
    bool tipo_I = false;
    char buffer[TAM_BUFFER + 1] = "";
    argsglobal = (struct arg1 *) arg;
    argsglobal->pass = false;
    int socket_conexao = argsglobal->socket_conexao;
    struct sockaddr_in descritor_conexao = argsglobal->descritor_conexao;
    argsglobal->alocado=false;

    //Enquanto está escutando na porta de conexão
    while(true){
        tipo_I = false;
        //Aceita conexão
        int conexao = accept(socket_conexao, NULL, NULL);
        
        //Tratamento de erros
        if(conexao < 0) {
            fprintf(stderr, "Erro: Falha de conexão [accept].\n");
            close(socket_conexao);
            exit(1);
        }
        mensagens_servidor(220, conexao, NULL);

        //Tratamento de erros
        if(autenticacao(conexao)) {
            fprintf(stderr, "Erro: Falha de login.\n");
            shutdown(conexao, socket_conexao);
        }

        tamanho_comando = read(conexao, buffer, TAM_BUFFER -1);
        while(tamanho_comando > 0) {

            //Adiciona terminador de string \0
            buffer[tamanho_comando] = '\0';
            //Comparações para definir comando recebido
            //Necessário \r\n no final, sendo que cada
            //vale como um caracter
            //Comandos que levam argumentos tem espaço
            //(entre o comando e o argumento, que é lido
            //depois), então não têm \r\n
            printf("-----------------------------------------------------------\n");
            printf("%s", buffer);
            printf("-----------------------------------------------------------\n");
            if (strncmp(buffer, "NOOP\r\n", 6) == 0) {
                mensagens_servidor(220, conexao, NULL);
            } else if(strncmp(buffer, "SYST\r\n", 6) == 0) {

                mensagens_servidor(215, conexao, NULL);
            } else if(strncmp(buffer, "QUIT\r\n", 6) == 0) {
                quit(conexao);
                
            } 
            else if(strncmp(buffer, "MODE ", 5) == 0) {
                if(strncmp(buffer +5, "S\r\n", 3) == 0){
                    mensagens_servidor(200, conexao, NULL);
                } else {
                    //Erro, pois só foi implementado o modo
                    //stream (S), seguindo a especificação
                    //de implementação mínima dada pelo RFC 959
                    mensagens_servidor(504, conexao, NULL);
                }
            } else if(strncmp(buffer, "TYPE ", 5) == 0) {
                if(strncmp(buffer +5, "I\r\n", 3) == 0){
                    tipo_I = true;
                }
                mensagens_servidor(200, conexao, NULL);
            } else if(strncmp(buffer, "STRU ", 5) == 0) {
                mensagens_servidor(200, conexao, NULL);
            } else if(strncmp(buffer, "PORT ", 5) == 0) {
                port(buffer, &descritor_conexao, conexao);
                mensagens_servidor(200, conexao, NULL);

            } else if(strncmp(buffer, "PASV", 4) == 0) 
            {
                mensagens_servidor(227, conexao, pasv(argsglobal)); 
                  
            } else if(strncmp(buffer, "LIST", 4) == 0) {
                mensagens_servidor(150, conexao, NULL);
                list(&descritor_conexao,strtok(buffer +4, "\r\n"));
                mensagens_servidor(226, conexao, NULL);
            } else if(strncmp(buffer, "RETR ", 5) == 0) {
                if(tipo_I) {
                    if(get(conexao, &descritor_conexao, strtok(buffer +5, "\r\n")) > 0) {
                        mensagens_servidor(550, conexao, NULL);
                    } else {
                        mensagens_servidor(226, conexao, NULL);
                    }
                } else {
                    mensagens_servidor(451, conexao, NULL);
                }
            } else if(strncmp(buffer, "STOR ", 5) == 0) {
                if(tipo_I) {
                    if(put(conexao, &descritor_conexao, strtok(buffer +5, "\r\n")) > 0) {
                        mensagens_servidor(550, conexao, NULL);
                    } else {
                        mensagens_servidor(226, conexao, NULL);
                    }
                } else {
                    mensagens_servidor(451, conexao, NULL);
                }
            } else {
                fprintf(stderr, "Erro: Comando não suportado.\n");
                mensagens_servidor(202, conexao, NULL);
            }
            tamanho_comando = read(conexao, buffer, TAM_BUFFER -1);
        }
        //Fim do while é o fim da transação com o cliente.
        shutdown(conexao, socket_conexao);

        //Limpa o buffer
        memset(buffer, 0, sizeof(buffer));
    }
}

void quit(int conexao) {
    
    printf("Conexão de %s encerrada.\n", argsglobal->IPcliente);
    fflush(stdout);
    atualizaTaxa(2, argsglobal->taxaCliente);
    mensagens_servidor(221, conexao, NULL);
    shutdown(conexao, SHUT_RDWR);
    close(conexao);
}

//função para atualização da taxa disponível do servidor.
//Decrementa quando aceita novo cliente e incrementa quando o cliente sai.
bool atualizaTaxa(int param, float taxa){
    float taxaDisp;
    float temp;
    char tempTaxdisp[TAM_BUFFER];

    FILE* tfile = NULL;
    tfile = fopen(".taxaserv", "r");
    fgets(tempTaxdisp, TAM_BUFFER, tfile);
    sscanf(tempTaxdisp, "%f", &temp);
    fclose (tfile);

    if (param == 1){
        taxaDisp = temp - taxa;
        if (taxaDisp>=0){
            sprintf(tempTaxdisp, "%f", taxaDisp);
            tfile = fopen(".taxaserv", "w");
            fwrite(tempTaxdisp, 1, strlen(tempTaxdisp), tfile);
            fclose(tfile);
            return true;
        }
        else
            return false;
    }
    else if (param == 2){
        taxaDisp = temp + taxa;
        sprintf(tempTaxdisp, "%f", taxaDisp);
        tfile = fopen(".taxaserv", "w");
        fwrite(tempTaxdisp, 1, strlen(tempTaxdisp), tfile);
        fclose(tfile);
        return true;
    }//falta else que devolve taxa disponível para o servidor
}