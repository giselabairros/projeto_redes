//Integrantes: Gisela Bairros e Guilherme Halal
//Servidor FTP

#include "servidor.h"

//Método principal
int main(int argc, char *argv[]) {

    if(argc < 2) {
        fprintf(stderr, "Erro: Necessário informar o número da porta.\n");
        return -1;
    }else if(argc < 3) {
        fprintf(stderr, "Taxa do servidor não definida. Necessário definir taxa do servidor.\n");
        return -1;
    }

    unsigned int porta = atoi(argv[1]);
    float taxaPadrao = atof(argv[2]);
    taxaPadraoCliente = taxaPadrao/4;
    ips= 0;
    

    float taxaRestante;

    //criando arquivo para controle da taxa máxima do servidor:
    remove("./.taxaserv");
    
    FILE* tfile = NULL;
    tfile = fopen(".taxaserv", "w");
    
    char temp[16];
    sprintf(temp, "%f", taxaPadrao);
    
    fwrite(temp, 1, strlen(temp), tfile);
    fclose(tfile);

    //armazena arquivo com restrições por IP

    FILE* ipfile = NULL;
    ipfile = fopen(".ipsFTP", "r");
    
    if(ipfile==NULL) {
        printf("Arquivo de clientes nao pode ser aberto. será usara taxa padrao %1.f kbps para qualquer cliente.\n", taxaPadraoCliente);
        fflush(stdout);
    }
    
    char tempIPTax[TAM_BUFFER];
    
    char tempIP[15];
    float tempTaxa;
    while(fgets(tempIPTax, TAM_BUFFER, ipfile)) {
        sscanf(tempIPTax, "%s %f", taxIP[ips].ip, &taxIP[ips].taxa);
        ips++;
    }
    
    fclose(ipfile);


    //Cria sockets
    struct sockaddr_in endereco_socket_conexao, descritor_conexao;
    //Cria estrutura que lida com endereços de rede, sendo sockaddr_in,
    //especificamente, para lidar com IPv4
    inicializa_sockaddr_in(&endereco_socket_conexao, NULL, porta);
    //Cria socket e conexão de controle, pois a conexão de dados só vai
    //ser aberta quando for feita leitura ou escrita.
    int socket_conexao = cria_socket(&endereco_socket_conexao);

    //Tratamento de erros
    if(socket_conexao < 0){
        fprintf(stderr, "Erro: criação do socket falhou.\n");
        return -1;
    }

    //Inicia escuta na porta de conexão
    //com tratamento de erros
    if(listen(socket_conexao, MAX_CONEXOES) == -1) {
        fprintf(stderr, "Erro: Escuta falhou.\n");
        close(socket_conexao);
        return -1;
    }

    //pthread recebe argumentos no formato de structs para ser genérico,
    //então aqui as informações do servidor são passadas para uma struct
    struct arg1 args;
    args.socket_conexao = socket_conexao;
    args.descritor_conexao = descritor_conexao;
    

    //Criação de threads
    pthread_t thread1;
    pthread_t thread2;
    pthread_t thread3;
    pthread_t thread4;

    pthread_create(&thread1, NULL, aceitando_conexoes, (void*)&args);
    pthread_create(&thread2, NULL, aceitando_conexoes, (void*)&args);
    pthread_create(&thread3, NULL, aceitando_conexoes, (void*)&args);
    pthread_create(&thread4, NULL, aceitando_conexoes, (void*)&args);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    pthread_join(thread3, NULL);
    pthread_join(thread4, NULL);

    close(socket_conexao);
    return 0;
}