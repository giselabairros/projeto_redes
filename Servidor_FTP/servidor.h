//Integrantes: Gisela Bairros e Guilherme Halal
//Servidor FTP

#ifndef SERVIDOR_H
#define SERVIDOR_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h> 
#include <netdb.h>
#include <pthread.h>
#include <net/if.h>
#include <sys/ioctl.h>
//Structs de argumentos para pthreads
struct arg1{
  int socket_conexao;
  int socket_dados;
  char IPcliente[16]; 
  struct sockaddr_in descritor_conexao;
  float taxaCliente;
  bool pass;
  bool alocado;
};

struct taxaVSips{
  char ip[15];
  float taxa;
};
	
struct arg1 *argsglobal;
float taxaPadraoCliente;
struct taxaVSips taxIP[15];
int ips;


#define PORTA_DADOS 3242

//Tamanho do buffer
#define TAM_BUFFER 256

//Máximo de conexões concorrentes aceitas
#define MAX_CONEXOES 10

//Métodos:
void mensagens_servidor(int num_msg, int conexao, char *info);
void inicializa_sockaddr_in(struct sockaddr_in *endereco_socket_conexao, char *endereco_IP, unsigned int porta);
int cria_socket(struct sockaddr_in *endereco_socket);
int autenticacao(int conexao);
void port(char *buffer, struct sockaddr_in *endereco_cliente, int conexao);
void list(const struct sockaddr_in *endereco_cliente, char *diretorio);
int cria_conexao_dados(const struct sockaddr_in *endereco_cliente);
int get(int conexao, const struct sockaddr_in *endereco_cliente, char *nome_arquivo);
int put(int conexao, const struct sockaddr_in *endereco_cliente, char *nome_arquivo);
void *aceitando_conexoes (void *arg);
bool atualizaTaxa(int param, float taxa);
void quit(int conexao);
char *pasv(struct arg1 *endereco_cliente); //não precisa de argumentos aparentemente


#endif